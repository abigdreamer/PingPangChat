package com.pingpang.websocketchat;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ResourceUtils;

import com.pingpang.util.SslUtil;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * 服务端 ChannelInitializer
 */
public class WebsocketChatServerInitializer extends ChannelInitializer<SocketChannel> {
	
	//日志操作
    private Logger logger = LoggerFactory.getLogger(WebsocketChatServerInitializer.class);
	
	@Override
    public void initChannel(SocketChannel ch) throws Exception {
		ChannelPipeline pipeline = ch.pipeline();

		pipeline.addLast(new IdleStateHandler(10, 0, 0, TimeUnit.SECONDS));

		pipeline.addLast(new HttpServerCodec());
		pipeline.addLast(new HttpObjectAggregator(1024 * 1024));
		pipeline.addLast(new ChunkedWriteHandler());
		// pipeline.addLast(new HttpRequestHandler("/ws"));
		pipeline.addLast(new WebSocketServerProtocolHandler("/ws", null, true, 1024 * 512));
		// pipeline.addLast(new HeartbeatServerHandler());
		pipeline.addLast(new TextWebSocketFrameHandler());
		// 配置ssl访问的
//		logger.info(WebsocketChatServerInitializer.class.getResource("keystore.p12").toString());
//		URL xmlpath = this.getClass().getClassLoader().getResource("keystore.p12");
//		logger.info(xmlpath.toString());
//		// File f = new File(this.getClass().getResource("/").getPath());
//		File f = new File(WebsocketChatServerInitializer.class.getResource("keystore.p12").toString());
//		logger.info(f.getAbsolutePath());
        
//		logger.info("SSL/TLS path:"+ResourceUtils.getFile("classpath:keystore.p12").getPath());
		
//		ClassPathResource cpr=new ClassPathResource("keystore.p12");
		InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("keystore.p12");
        
		SSLContext sslContext = SslUtil.createSSLContext("PKCS12",
                                inputStream, "changeit"); // SSLEngine engine =
		sslContext.createSSLEngine();
		SSLEngine sslEngine = sslContext.createSSLEngine();
		sslEngine.setNeedClientAuth(false);
		sslEngine.setUseClientMode(false);
		logger.info(sslContext.getProtocol());
		logger.info("支持的协议: " + Arrays.asList(sslEngine.getSupportedProtocols()));
		logger.info("启用的协议: " + Arrays.asList(sslEngine.getEnabledProtocols()));
		logger.info("支持的加密套件: " + Arrays.asList(sslEngine.getSupportedCipherSuites()));
		logger.info("启用的加密套件: " + Arrays.asList(sslEngine.getEnabledCipherSuites()));
		pipeline.addFirst(new SslHandler(sslEngine));

    }
}
