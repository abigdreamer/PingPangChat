package com.pingpang.redis.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.pingpang.util.JsonFilterUtil;


@Configuration
public class RedisConfig {
	
	 @Bean
	    public RedisTemplate<String, Object> redisTemplateString(RedisConnectionFactory redisConnectionFactory) {
	        // 配置redisTemplate
	        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<String, Object>();
	        redisTemplate.setConnectionFactory(redisConnectionFactory);
	        RedisSerializer stringSerializer = new StringRedisSerializer();
	        redisTemplate.setKeySerializer(stringSerializer); // key序列化
	        redisTemplate.setValueSerializer(stringSerializer); // value序列化
	        redisTemplate.setHashKeySerializer(stringSerializer); // Hash key序列化
	        redisTemplate.setHashValueSerializer(stringSerializer); // Hash value序列化
	        redisTemplate.afterPropertiesSet();
	        return redisTemplate;
	    }
	
	
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        // 配置redisTemplate
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<String, Object>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        RedisSerializer stringSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringSerializer); // key序列化
        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer()); // value序列化
        redisTemplate.setHashKeySerializer(stringSerializer); // Hash key序列化
        redisTemplate.setHashValueSerializer(new GenericJackson2JsonRedisSerializer()); // Hash value序列化
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }
    
    
    @Bean
    public RedisTemplate<String, Object> redisTemplateGroup(RedisConnectionFactory redisConnectionFactory) {
        // 配置redisTemplate
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<String, Object>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        RedisSerializer stringSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringSerializer); // key序列化
        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer(getMapper())); // value序列化
        redisTemplate.setHashKeySerializer(stringSerializer); // Hash key序列化
        redisTemplate.setHashValueSerializer(new GenericJackson2JsonRedisSerializer(getMapper())); // Hash value序列化
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }
    
 // 获取JSON工具
   	private final ObjectMapper getMapper() {
   		ObjectMapper mapper = new ObjectMapper();
   		JsonFilterUtil.addFilterForMapper(mapper);
   		//将类名称序列化到json串中，去掉会导致得出来的的是LinkedHashMap对象，直接转换实体对象会失败
   		mapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance,ObjectMapper.DefaultTyping.NON_FINAL); 	    //设置输入时忽略JSON字符串中存在而Java对象实际没有的属性
   		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
   		return mapper;
   	}
   	   
   	 //表示监听一个频道
   	/**
	 * redis消息监听器容器
	 * 可以添加多个监听不同话题的redis监听器，只需要把消息监听器和相应的消息订阅处理器绑定，该消息监听器
	 * 通过反射技术调用消息订阅处理器的相关方法进行一些业务处理
	 * @param connectionFactory
	 * @param listenerAdapter
	 * @return
	 */
	//MessageListenerAdapter 表示监听频道的不同订阅者
	/*
	 * @Bean RedisMessageListenerContainer container(RedisConnectionFactory
	 * connectionFactory, MessageListenerAdapter listenerAdapter){
	 * RedisMessageListenerContainer container = new
	 * RedisMessageListenerContainer();
	 * container.setConnectionFactory(connectionFactory); //订阅多个频道
	 * container.addMessageListener(listenerAdapter,new
	 * PatternTopic(RedisPre.SEND_MSG)); //序列化对象（特别注意：发布的时候需要设置序列化；订阅方也需要设置序列化）
	 * Jackson2JsonRedisSerializer seria = new Jackson2JsonRedisSerializer
	 * (Object.class);
	 * 
	 * ObjectMapper mapper = new ObjectMapper();
	 * //将类名称序列化到json串中，去掉会导致得出来的的是LinkedHashMap对象，直接转换实体对象会失败
	 * //mapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance,
	 * ObjectMapper.DefaultTyping.NON_FINAL); //设置输入时忽略JSON字符串中存在而Java对象实际没有的属性
	 * mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	 * 
	 * seria.setObjectMapper(mapper); container.setTopicSerializer(seria);
	 * 
	 * return container; }
	 */

	
	// 表示监听一个频道

	/*
	 * @Bean public MessageListenerAdapter listenerAdapter(RedisService receiver) {
	 * // 这个地方 是给messageListenerAdapter 传入一个消息接受的处理器， return new
	 * //MessageListenerAdapter(receiver, "chatMsg"); }
	 */
	 
}
