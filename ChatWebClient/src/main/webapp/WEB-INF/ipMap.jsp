<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>访问统计</title>
<script type="text/javascript"
	src="${httpServletRequest.getContextPath()}/map/echarts.js"></script>
<script type="text/javascript"
	src="${httpServletRequest.getContextPath()}/map/esl.js"></script>
<script type="text/javascript"
	src="${httpServletRequest.getContextPath()}/map/config.js"></script>
<script type="text/javascript"
	src="${httpServletRequest.getContextPath()}/map/jquery.min.js"></script>
<script type="text/javascript"
	src="${httpServletRequest.getContextPath()}/map/facePrint.js"></script>
<script type="text/javascript"
	src="${httpServletRequest.getContextPath()}/map/testHelper.js"></script>
<%-- <script type="text/javascript"
	src="${httpServletRequest.getContextPath()}/map/china.js"></script> --%>
<link rel="stylesheet"
	href="${httpServletRequest.getContextPath()}/map/reset.css">
</head>
<body>
	<div id="myChart" style="width: 1300px; height: 900px;"></div>
</body>
	<script type="text/javascript">
	var name_title = "访问地区分布";
    var subname = '';
    var nameColor = '#4444FF';
    var name_fontFamily = '等线';
    var name_fontSize = 25;
    var subname_fontSize = 15;
    var mapName = 'china';
    var data=[];
    
function getInitIP(){
    $.ajax({
	    url:"${httpServletRequest.getContextPath()}/userController/ipMap",
	    type:"Post",
	    async:false,//同步
	    dataType:"json",
	    success:function(ipData){
	    	//console.log(ipData);
	    	data=ipData;
	    }
    });
}
    
    var echarts;
    var colorTool;
    var myChart;
    var mapFeatures;
    var geoCoordMap = {};
    
 // 用于处理effectScatter气泡散点图，气泡的大小
    var max = 50,
        min = 1;
    var maxSize4Pin = 100,
        minSize4Pin = 20;
    //require('${httpServletRequest.getContextPath()}/map/china.js').EVENT;
    //echarts.registerMap(mapName, require('${httpServletRequest.getContextPath()}/map/china.json'));
    require([
        'echarts',
        '${httpServletRequest.getContextPath()}/map/china'
        // 'zrender/tool/color',
        // 'echarts/chart/line',
        // 'echarts/chart/map',
        // 'echarts/component/title',
        // 'echarts/component/geo',
        // 'echarts/component/legend',
        // 'echarts/component/tooltip',
        // 'echarts/component/toolbox',
        // 'echarts/component/visualMap'
    ], function (ec, ct) {
    	getInitIP();
        echarts = ec;
        myChart = echarts.init(document.getElementById('myChart'));
        //$.get('${httpServletRequest.getContextPath()}/map/china.json', function (worldJson) {
        //echarts.registerMap(mapName, worldJson);
        
        mapFeatures = echarts.getMap(mapName).geoJson.features;
        mapFeatures.forEach(function (v) {
          // 地区名称
          var name = v.properties.name;
          // 地区经纬度
          geoCoordMap[name] = v.properties.cp;
        });
        
        // 柱状图数据处理
        var yData = [];
        var barData = data.filter(function (item) {
          return item.value > 0;
        });
        barData = barData.sort(function (a, b) {
          return b.value - a.value;
        });
        for (var j = 0; j < barData.length; j++) {
          if (j < 10) {
            yData.push('0' + j + barData[j].name);
          } else {
            yData.push(j + barData[j].name);
          }
        }
        
     // ECharts配置
        var option = {
          // 标题
          title: {
            text: name_title,
            subtext: subname,
            left: '300',
            top: 30,
            textStyle: {
              color: nameColor,
              fontFamily: name_fontFamily,
              fontSize: name_fontSize
            },
            subtextStyle: {
              fontSize: subname_fontSize,
              fontFamily: name_fontFamily
            }
          },
          // 提示框信息
          tooltip: {
            trigger: 'item',
            formatter: function (params) {
              var toolTiphtml = '';
              if (params.name === '南海诸岛') {
                toolTiphtml = '';
              } else if (typeof (params.value)[2] === "undefined") {
                toolTiphtml = params.value === 0 ? '' : (transferProvinceName(params.name) + ': ' + params.value);
              } else {
                toolTiphtml = transferProvinceName(params.name) + ': ' + params.value[2];
              }
              return toolTiphtml;
            }
          },
          // 视觉映射，数据范围Low--High
          visualMap: {
            show: true,
            min: 0,
            max: 100,
            left: 'left',
            top: 30,
            //top: 'bottom',
            text: ['High', 'Low'], // 文本，默认为数值文本
            calculable: true,
            seriesIndex: [1],
            // 数据颜色范围，值越大颜色越深
            inRange: {
              color: ['#EFEFFF', '#4444FF']
            }
          },
          grid: {
            right: 25,
            top: 80,
            bottom: 20,
            width: '200'
          },
          xAxis: {
            show: false
          },
          yAxis: {
            type: 'category',
            inverse: true,
            nameGap: 16,
            axisLine: {
              show: false,
              lineStyle: {
                color: '#ddd'
              }
            },
            axisTick: {
              show: false,
              lineStyle: {
                color: '#ddd'
              }
            },
            // Y轴刻度标签，富文本展示
            axisLabel: {
              interval: 0,
              margin: 105,
              textStyle: {
                align: 'left',
                fontSize: 14
              },
              rich: {
                // 前三名, 序号颜色
                a: {
                  color: '#fff',
                  backgroundColor: '#f0515e',
                  width: 20,
                  height: 20,
                  align: 'center',
                  borderRadius: 2
                },
                // 第四名之后, 序号颜色
                b: {
                  color: '#fff',
                  backgroundColor: '#24a5cd',
                  width: 20,
                  height: 20,
                  align: 'center',
                  borderRadius: 2
                },
                // 前三名, 文字颜色
                x: {
                  color: '#f0515e'
                },
                // 第四名之后, 文字颜色
                y: {
                  color: '#24a5cd'
                }
              },
              // 处理前三名, 及其他的标签
              formatter: function (params) {
                if (parseInt(params.slice(0, 2)) < 3) {
                  return [
                    '{a|' + (parseInt(params.slice(0, 2)) + 1) + '}' + '  ' + '{x|' + transferProvinceName(params.slice(2)) + '}'
                  ].join('\n')
                } else {
                  return [
                    '{b|' + (parseInt(params.slice(0, 2)) + 1) + '}' + '  ' + '{y|' + transferProvinceName(params.slice(2)) + '}'
                  ].join('\n')
                }
              }
            },
            data: yData
          },
          // 地理坐标系组件,绘制地图,散点图等
          geo: {
            show: true,
            map: mapName,
            top: 100,
            left: 'left',
            right: '350',
            label: {
              normal: {
                show: false
              },
              emphasis: {
                show: false,
              }
            },
            roam: true,
            itemStyle: {
              // 地图hover颜色
              // emphasis: {
              //   areaColor: '#4444FF',
              // }
            },
            //是否显示南海诸岛
            regions: [{
              name: "南海诸岛",
              value: 0,
              itemStyle: {
                normal: {
                  areaColor: '#EFEFFF',
                  // opacity为0不绘制该图形
                  opacity: 1,
                  label: {
                    show: false
                  },
                }
              }
            }],
          },
          series: [
            // 散点图, 蓝色的点
            {
              name: '散点图',
              type: 'scatter',
              coordinateSystem: 'geo',
              data: convertData(data),
              symbolSize: function (val) {
                return val[2] / 10;
              },
              label: {
                normal: {
                  formatter: function (obj) {
                    let name = transferProvinceName(obj.name);
                    return name || '';
                  },
                  position: 'right',
                  show: true
                },
                emphasis: {
                  show: true
                }
              },
              itemStyle: {
                normal: {
                  color: '#4444FF'
                }
              }
            },
            // 地图
            {
              name: 'IP分布',
              type: 'map',
              mapType: 'china',
              selectedMode : 'multiple',
              geoIndex: 0,
              aspectScale: 0.75, // 地图长宽比
              showLegendSymbol: false,
              label: {
                normal: {
                  show: true
                },
                emphasis: {
                  show: false,
                  textStyle: {
                    color: '#fff'
                  }
                }
              },
              roam: true,
              // itemStyle: {
              //   normal: {
              //     areaColor: '#031525',
              //     borderColor: '#3B5077',
              //   },
              //   emphasis: {
              //     areaColor: '#2B91B7'
              //   }
              // },
              animation: false,
              data: data
            },
            // 气泡散点图，红色气泡
            {
              name: '气泡散点图',
              type: 'scatter',
              coordinateSystem: 'geo',
              // 'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow', 'none'
              symbol: 'pin', //气泡
              symbolSize: function (val) {
                // 根据value值，设置symbol大小，根据实际情况自己调整
                if (val[2] === 0) {
                  return 0;
                }
                var a = (maxSize4Pin - minSize4Pin) / (max - min);
                var b = maxSize4Pin - a * max;
                return a * val[2] + b * 1.2;
              },
              label: {
                normal: {
                  show: true,
                  formatter: function (obj) {
                    return obj.data.value[2];
                  },
                  textStyle: {
                    color: '#fff',
                    fontSize: 9,
                  }
                }
              },
              itemStyle: {
                normal: {
                  // 气泡颜色
                  color: '#F62157', 
                }
              },
              zlevel: 6,
              data: convertData(data),
            },
            // 前五名，带有涟漪特效动画的散点（气泡）图，黄色
            {
              name: 'Top 5',
              type: 'effectScatter',
              coordinateSystem: 'geo',
              data: convertData(data.sort(function (a, b) {
                return b.value - a.value;
              }).slice(0, 5)),
              symbolSize: function (val) {
                return val[2] / 1;
              },
              showEffectOn: 'render',
              rippleEffect: {
                brushType: 'stroke'
              },
              hoverAnimation: true,
              label: {
                normal: {
                  formatter: function (obj) {
                    let name = transferProvinceName(obj.name);
                    return name || '';
                  },
                  position: 'right',
                  show: false
                }
              },
              itemStyle: {
                normal: {
                  color: 'yellow',
                  shadowBlur: 10,
                  shadowColor: 'yellow'
                }
              },
              zlevel: 1
            },
            // 柱状图
            {
              name: '柱状图',
              type: 'bar',
              roam: false,
              visualMap: false,
              barMaxWidth: 20,
              zlevel: 2,
              barGap: 0,
              itemStyle: {
                normal: {
                  // 柱状图，渐变色
                  color: function (params) {
                    var colorList = [{
                      colorStops: [{
                        offset: 0,
                        color: '#f0515e'
                      }, {
                        offset: 1,
                        color: '#ef8554'
                      }]
                    },
                    {
                      colorStops: [{
                        offset: 0,
                        color: '#3c38e4'
                      }, {
                        offset: 1,
                        color: '#24a5cd'
                      }]
                    }
                    ];
                    if (params.dataIndex < 3) {
                      return colorList[0]
                    } else {
                      return colorList[1]
                    }
                  },
                },
                // 柱状图hover颜色
                emphasis: {
                    color: "#f0515e"
                },
              },
              label: {
                normal: {
                  show: true,
                  position: 'right',
                  textBorderWidth: 0
                }
              },
              data: barData
            }
          ]
        };
        //chart = testHelper.createChart(echarts, 'myChart', option);
        myChart.setOption(option);
        //}); 
    }); 
    
    // 将中文省份名称转换为拼音名称
    function transferProvinceName(name) {
      return name;
      /* var provincesPinyin = ['Shanghai', 'Hebei', 'Shanxi', 'Neimenggu', 'Liaoning', 'Jilin', 'Heilongjiang', 'Jiangsu', 'Zhejiang', 'Anhui', 'Fujian', 'Jiangxi', 'Shandong', 'Henan', 'Hubei', 'Hunan', 'Guangdong', 'Guangxi', 'Hainan', 'Sichuan', 'Guizhou', 'Yunnan', 'Xizang', 'Shanxi1', 'Gansu', 'Qinghai', 'Ningxia', 'Xinjiang', 'Beijing', 'Tianjin', 'Chongqing', 'Xianggang', 'Aomen', 'Taiwan'];
      var provincesChinese = ['上海', '河北', '山西', '内蒙古', '辽宁', '吉林', '黑龙江', '江苏', '浙江', '安徽', '福建', '江西', '山东', '河南', '湖北', '湖南', '广东', '广西', '海南', '四川', '贵州', '云南', '西藏', '陕西', '甘肃', '青海', '宁夏', '新疆', '北京', '天津', '重庆', '香港', '澳门', '台湾'];
      var pinyinName = provincesPinyin[provincesChinese.indexOf(name)];
      return pinyinName || ''; */
    }

    // 用于处理散点图数据
    function convertData(data) {
      var res = [];
      for (var i = 0; i < data.length; i++) {
        var geoCoord = geoCoordMap[data[i].name];
        if (geoCoord) {
          res.push({
            name: data[i].name,
            value: geoCoord.concat(data[i].value),
          });
        }
      }
      return res;
    };
    
    function refreshData(){
        if(!myChart){
             return;
        }
        getInitIP();
        //更新数据
         var option = myChart.getOption();
        /*  console.log(option.series[0].name);
         console.log(option.series[1].name);
         console.log(option.series[2].name);
         console.log(option.series[3].name);
         console.log(option.series[4].name); */
         option.series[0].data = convertData(data);  
         option.series[1].data = data;
         option.series[2].data = convertData(data);
         option.series[3].data = convertData(data.sort(function (a, b) {
             return b.value - a.value;
         }).slice(0, 5));
      
         // 柱状图数据处理
         var yData = [];
         var barData = data.filter(function (item) {
           return item.value > 0;
         });
         barData = barData.sort(function (a, b) {
           return b.value - a.value;
         });
         for (var j = 0; j < barData.length; j++) {
           if (j < 10) {
             yData.push('0' + j + barData[j].name);
           } else {
             yData.push(j + barData[j].name);
           }
         }
         
         option.series[4].data = barData;
         
         option.yAxis[0].data = yData;
         
         myChart.setOption(option);    
   }
    
    setInterval(refreshData ,5000);
</script>
</html>