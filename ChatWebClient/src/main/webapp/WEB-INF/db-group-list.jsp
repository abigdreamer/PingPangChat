<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="x-admin-sm">
     <head>
        <meta charset="UTF-8">
        <title>PingPang管理页面</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/font.css">
        <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/xadmin.css">
        <script src="${httpServletRequest.getContextPath()}/layer/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="${httpServletRequest.getContextPath()}/x-admin/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5" id="sreach">
                                <div class="layui-input-inline layui-show-xs-block">
                                 <!-- 信息状态0未发送 1已发送 -1删除 -->
                                    <select name="groupStatus">
                                        <option value="">所有状态</option>
                                        <option value="0">正常</option>
                                        <option value="-1">作废</option>
                                    </select>
                                </div>
                                
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="groupUserCode"  placeholder="用户代码" autocomplete="off" class="layui-input">
                                </div>
                                
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input"  autocomplete="off" placeholder="开始日" name="startDate" id="start">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input"  autocomplete="off" placeholder="截止日" name="endDaet" id="end">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <input type='button' class="layui-btn"  onclick="sreach();" value="查询">
                                </div>
                            </form>
                        </div>
                        <div class="layui-card-header">
                            <button class="layui-btn layui-btn-xs" onclick="managerAll(-1)"><i class="layui-icon"></i>批量禁用</button>
                            <button class="layui-btn layui-btn-warm" onclick="managerAll(0)"><i class="layui-icon"></i>批量正常</button>
                            <button class="layui-btn" onclick="xadmin.open('添加群组','${httpServletRequest.getContextPath()}/group/add-group-index',600,400)"><i class="layui-icon"></i>添加</button>
                        </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form" id="groupList">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </body>
    <script>
    layui.use(['laydate','form','table'],
            function() {
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '#start' //指定元素
                });

                //执行一个laydate实例
                laydate.render({
                    elem: '#end' //指定元素
                });
                
                var table = layui.table;
                //第一个实例
                table.render({
                  elem: '#groupList',
                  height: 312,
                  url: '${httpServletRequest.getContextPath()}/group/db-list', //数据接口
                  method:'post',
                  contentType: 'application/json; charset=UTF-8',
                  initSort: {
                	    field: 'groupCreateDate' //排序字段，对应 cols 设定的各字段名
                	    ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
                	  },
                  //where:JSON.stringify(layui.form.field),
                  page: true, //开启分页
                  cols: [
                	      [
                	    	{type: 'checkbox',field: 'id',fixed:'left'},
    		                {field: 'groupCode', title: '群编码', width:90},
    		                {field: 'groupName', title: '群名称', width:90},
    		                {field: 'groupPurpose', title: '群备注', width:90},
    		                {field: 'groupUserCode', title: '用户编码', width:90},
    		                {field: 'groupCreateDate', title: '创建日期', width:90},
    		                {field: 'groupStatus', title: '状态', width: 90,templet:'<div>{{ statusFormat(d.groupStatus)}}</div>'},
    		                {field: 'right', title: '操作', width: 370, toolbar:"#barDemo"}
                          ]
                	    ]
                });
                
            });
    
    //状态翻译 信息状态0未发送 1已发送 -1删除
    function statusFormat(statu) {
    	var msg="";
        if(-1==statu){
        	msg="禁用";
        }else if(0==statu){
        	msg="正常";
        }else{
        	msg=" 未识别 ";
        }
        return msg;
    }  
    
    /**
     *type 状态-1禁用 0正常
     */
    function managerMsg(id,type){
    	 var msg="";
    	 var url="${httpServletRequest.getContextPath()}/group/db-manager";
    	 if(0==type){
    		 msg="确认启用吗？";
    	 }else if(-1==type){
    		 msg="确认要禁用吗？";
    	 }else{
    		 layer.msg('操作未识别!', {icon: 1});
   		     return false;
    	 }
    	 
    	 layer.confirm(msg,function(index){
       	  $.ajax({
       		  type: 'POST',
       		  async: false,
       		  dataType:'json',
       		  url:url,
       		  data:{ids:id,status:type},
       		  success: function(data){
       			  if("S"==data.CODE){
       				  layer.msg('执行成功', {icon: 1});
       				 // window.parent.location.reload();
       				  layui.table.reload("groupList", { //此处是上文提到的 初始化标识id
       		                where: {
       		                    //key: { //该写法上文已经提到
       		                        //type: item.type, id: item.id
       		                    //}
       		                }
       		            });
       			  }else{
       				if(null==data.MESSAGE){
       				  layer.msg('执行失败', {icon: 1});
       				}else{
       				  layer.msg(data.MESSAGE, {icon: 1});
       				}
       			  }
       		  },
       		  error : function(errorMsg) {
                     layer.msg(errorMsg,{time:2000,end:function(){
       			  }});
       		  }
       		});
       });
    }
    
    /**
     *type 状态0未发送 1已发送 -1删除
     */
    function managerAll (type) {
    	var msg="";
   	    var url="${httpServletRequest.getContextPath()}/group/db-manager";
   	    if(0==type){
   		 msg="确认启用吗？";
   	    }else if(-1==type){
   		 msg="确认要禁用吗？";
   	    }else{
   		 layer.msg('操作未识别!', {icon: 1});
  		     return false;
   	    }
   	    
        var checkStatus = layui.table.checkStatus('groupList').data;
         var ids = [];
         for(var i=0;i<checkStatus.length;i++){
       	  ids.push(checkStatus[i].id);
         }
         layer.confirm(msg,function(index){
             //捉到所有被选中的，发异步进行删除
              $.ajax({
               		  type: 'POST',
               		  async: false,
               		  dataType:'json',
               		  url:url,
               		  data:{ids:ids.toString(),status:type},
               		  success: function(data){
               			  if("S"==data.CODE){
               				  layer.msg('执行成功', {icon: 1});
               				 // window.parent.location.reload();
               				  layui.table.reload("groupList", { //此处是上文提到的 初始化标识id
               		                where: {
               		                    //key: { //该写法上文已经提到
               		                        //type: item.type, id: item.id
               		                    //}
               		                }
               		            });
               			  }
               			  //layer.msg(data.MESSAGE,{time:2000,end:function(){
               			  //}});
               		  },
               		  error : function(errorMsg) {
                             layer.msg(errorMsg,{time:2000,end:function(){
               			  }});
               		  }
               		});
         }); 
       }
    
    function sreach(){
    	var param=JSON.stringify(getFormJson($('#sreach')));
    	//alert(param);
    	layui.table.reload("groupList", { //此处是上文提到的 初始化标识id
            where: {
            	search:param
            }
        });
    	return false;
    }
    

    //将form中的值转换为键值对。
    function getFormJson(frm) {
        var o = {dosubmit:1};
        var a = $(frm).serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }
    
    </script>
    <script type="text/html" id="barDemo">
        <button type="button" class="layui-btn layui-btn-xs" onclick="managerMsg('{{d.id}}','-1')">禁用</button>
		<button type="button" class="layui-btn layui-btn-warm" onclick="managerMsg('{{d.id}}','0')">正常</button>
</script>
</html>