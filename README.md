# PingPangChat

#### 介绍
基于netty的websocket即时聊天程序

#### 软件架构

聊天前台：layui

后台管理：X-admin

前端：weui+

后端框架：springboot2,netty,druid,mybatis,redis,nocos,dubbo

体验地址：https://175.178.63.183:8089/user/chat

         fhx/123456

         jz/123456

#### 功能界面

![输入图片说明](https://images.gitee.com/uploads/images/2022/0128/163922_f254ea04_62082.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0128/163935_2f2fd68b_62082.png "屏幕截图.png")

#### 群组聊天
![输入图片说明](readme/image099.png)

#### 图片传输和查看
![输入图片说明](readme/image001.png)

![输入图片说明](readme/image.png)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0128/163949_16693779_62082.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0128/164003_92baa71c_62082.png "屏幕截图.png")