package com.pingpang.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class StringUtil {

	/**
	 * 判断字符串是否为空
	 * @param str
	 * @return
	 */
	public static boolean isNUll(String str) {
		if(null==str || "".equals(str.trim()) || "null".equalsIgnoreCase(str)) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * 判断字符串是否为整数
	 * @param str
	 */
	public static boolean checkNum(String str) {
		if(StringUtil.isNUll(str)) {
			return false;
		}else {
			try {
			    Integer.valueOf(str);
			}catch(Exception e) {
				return false;
			}
			return true;
		}
	}
	
	/**
	 * 返回map数据
	 * @param CODE S:成功 F:失败
	 * @param MESSAGE
	 * @return
	 */
	public static Map<String,Object> returnMap(String code,String message){
		Map<String,Object> returnMap=new HashMap<String,Object>();
		returnMap.put("CODE", code);
		returnMap.put("MESSAGE", message);
		return returnMap;
	}
	
	/**
	 * 返回成功数据
	 * @return
	 */
	public static Map<String,Object> returnSucess(){
	   return returnMap("S","");
	}
	
	/**
	 * 返回失败数据 没返回信息的
	 * @return
	 */
	public static Map<String,Object> returnFail(){
		   return returnMap("F","");
      }
	
	/**
	 * MD5
	 * @param plainText
	 * @return
	 */
	public static String toMD5(String plainText) {
        String result = "";
        try {
            //生成实现指定摘要算法的 MessageDigest 对象。
            MessageDigest md = MessageDigest.getInstance("MD5");
            //使用指定的字节数组更新摘要。
            md.update(plainText.getBytes());
            //通过执行诸如填充之类的最终操作完成哈希计算。
            byte b[] = md.digest();
            //生成具体的md5密码到buf数组
            int i;
            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            result = buf.toString();
            //System.out.println("16位: " + buf.toString().substring(8, 24));// 16位的加密，其实就是32位加密后的截取
        } catch (Exception e) {
            e.printStackTrace();
        }
 
        return result;
    }
	
	/**
     * 依旧只创建一个 SimpleFormat 对象
     */
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
 
    // 采用加锁的方式，防止同步问题
    public static String format(Date date){
        synchronized (sdf){
            return sdf.format(date);
        }
    }
 
    public static Date parse(String date) throws ParseException {
        synchronized (sdf){
            return sdf.parse(date);
        }
    }
    
    
    /**
     * map转obj
     * @param <T>
     * @param map
     * @param cla
     * @return
     * @throws Exception
     */
	public static <T> Object mapToObject(Map<String, Object> map, Class<T> beanClass) throws Exception {
		T object = beanClass.newInstance();
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            int mod = field.getModifiers();
            if (Modifier.isStatic(mod) || Modifier.isFinal(mod)) {
                continue;
            }
            field.setAccessible(true);
            if (map.containsKey(field.getName())) {
                field.set(object, map.get(field.getName()));
            }
        }
        return object;
    }
	
    /**
     * object转换为map
     * @param obj
     * @return
     */
	public static Map<?, ?> objectToMap(Object object) {
		 Map<String, Object> map = new HashMap<String, Object>();
	        Field[] fields = object.getClass().getDeclaredFields();
	        for (Field field : fields) {
	            field.setAccessible(true);
	            try {
	            	 int mod = field.getModifiers();
	                 if (Modifier.isStatic(mod) || Modifier.isFinal(mod)) {
	                     continue;
	                 }
					map.put(field.getName(), field.get(object));
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
	        }
	        return map;
	}
}
